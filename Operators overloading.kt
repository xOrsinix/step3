import TimeInterval.*

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int)

// Supported intervals that might be added to dates:
enum class TimeInterval { DAY, WEEK, YEAR }


class RepeatedTimeInterval(val timeInterval: TimeInterval, val number: Int)

operator fun TimeInterval.times(number: Int) =
    RepeatedTimeInterval(this, number)

operator fun MyDate.plus(timeInterval: RepeatedTimeInterval): MyDate{
    return this.addTimeIntervals(timeInterval.timeInterval,timeInterval.number)
}

fun task1(today: MyDate): MyDate {
    return today + YEAR*1 + WEEK*1
}

fun task2(today: MyDate): MyDate {
    return today + YEAR * 2 + WEEK * 3 + DAY * 5
}